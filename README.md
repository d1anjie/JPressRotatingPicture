# JPressRotatingPicture

## 拆件介绍
JPress网页轮播图插件


## 使用方法

简单调用方法：(css样式请自行处理)

```html
#jpressAddonRotatingpictureList()
#for(jpressAddonRotatingpicture : jpressAddonRotatingpictureList)
<div class="swiper-slide">
    <img src="#(jpressAddonRotatingpicture.rotation_chart_ch)" class="pczs"/>
    <img  src="#(jpressAddonRotatingpicture.rotation_chart_en)" class="sjzs"/>
</div>
#end
#end
```

jpressAddonRotatingpicture 下的所有属性：

| 标签名称              | 作用           | 备注 |
| --------------------- | -------------- | ---- |
| id                    | banner图片id   |      |
| rotation_chart_ch     | 中文轮播图     |   图片链接,放到img的src下   |
| rotation_chart_en     | 英文轮播图     |  图片链接,放到img的src下    |
| rotation_chart_ch_url | 中文轮播图链接 |   跳转链接   |
| rotation_chart_en_url | 英文轮播图链接 |   跳转链接   |
| content_ch            | 中文轮播图描述 |   alt描述用   |
| content_en            | 英文轮播图描述 |   alt描述用   |


调用方法：`#(jpressAddonRotatingpicture.rotation_chart_ch)`