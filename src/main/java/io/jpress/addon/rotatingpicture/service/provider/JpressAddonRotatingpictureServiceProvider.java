package io.jpress.addon.rotatingpicture.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jpress.addon.rotatingpicture.service.JpressAddonRotatingpictureService;
import io.jpress.addon.rotatingpicture.model.JpressAddonRotatingpicture;
import io.jboot.service.JbootServiceBase;

@Bean
public class JpressAddonRotatingpictureServiceProvider extends JbootServiceBase<JpressAddonRotatingpicture> implements JpressAddonRotatingpictureService {

}