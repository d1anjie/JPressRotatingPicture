import io.jpress.codegen.AddonGenerator;

public class Codegen {

    private static String dbUrl = "jdbc:mysql://127.0.0.1:3306/jpress?useSSL=false";
    private static String dbUser = "root";
    private static String dbPassword = "anjie7410";

    private static String addonName = "rotatingpicture";
    private static String dbTables = "jpress_addon_rotatingpicture";
    private static String modelPackage = "io.jpress.addon.rotatingpicture.model";
    private static String servicePackage = "io.jpress.addon.rotatingpicture.service";


    public static void main(String[] args) {

        AddonGenerator moduleGenerator = new AddonGenerator(addonName, dbUrl, dbUser, dbPassword, dbTables, modelPackage, servicePackage);
        moduleGenerator.setGenUI(true);
        moduleGenerator.gen();

    }

}
