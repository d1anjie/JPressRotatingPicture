-- jpress.jpress_addon_rotatingpicture definition

CREATE TABLE `jpress_addon_rotatingpicture` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `rotation_chart_ch` varchar(225) DEFAULT NULL COMMENT '中文轮播图',
  `rotation_chart_en` varchar(225) DEFAULT NULL COMMENT '英文轮播图',
  `rotation_chart_ch_url` varchar(225) DEFAULT NULL COMMENT '中文轮播图链接',
  `rotation_chart_en_url` varchar(225) DEFAULT NULL COMMENT '英文轮播图链接',
  `content_ch` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `content_en` varchar(225) DEFAULT NULL COMMENT '英文描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
